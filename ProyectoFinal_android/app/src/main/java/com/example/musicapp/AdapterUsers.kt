package com.example.musicapp


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.android.synthetic.main.card_user.view.*
import android.widget.TextView
import com.squareup.picasso.Picasso

class AdapterUsers(private val userList: List<User>) : RecyclerView.Adapter<MoviewViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviewViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MoviewViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: MoviewViewHolder, position: Int) {
        val user: User = userList[position]
        holder.bind(user)
    }




    inner class AdapterUsers(itemView: View): RecyclerView.ViewHolder(itemView){
        fun setData(user: User, pos: Int){
            itemView.txtName.text = user.name
            itemView.txtLastName.text = user.last_name
            itemView.txtEmail.text = user.email
            //itemView.txt.text = movie.vote_average.toString()
        }
    }
}

class MoviewViewHolder (inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.card_user, parent, false)) {
    private var mName: TextView? = null
    private var mLastName: TextView? = null
    private var mEmail: TextView? = null
    private var mAvatar: ImageView? = null

    init {
        mName = itemView.findViewById(R.id.txtName)
        mLastName = itemView.findViewById(R.id.txtLastName)
        mEmail = itemView.findViewById(R.id.txtEmail)
        mAvatar = itemView.findViewById(R.id.imgAvatar)
    }

    fun bind(user: User){
        mName?.text = user.name
        mLastName?.text = user.last_name
        mEmail?.text = user.email
        Picasso.get()
            .load(user.avatar)
            .into(mAvatar)
    }

}