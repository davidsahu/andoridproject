package com.example.musicapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {

    private lateinit var txtUser : EditText
    private lateinit var txtPass : EditText
    private lateinit var btnSend : Button
    private lateinit var txtSignIn : TextView
    private lateinit var sesion : CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initElements()
    }

    private fun initElements(){
        txtUser = findViewById(R.id.txtUser)
        txtPass = findViewById(R.id.txtPass)
        btnSend = findViewById(R.id.btnSend)
        txtSignIn = findViewById(R.id.txtSignIn)
        sesion = findViewById(R.id.sesion)

        var prefs = getSharedPreferences("dataUser", MODE_PRIVATE)
        val sesionActive = prefs.getString("sesionUser", "no-data")
        //txtName.setText(usuario)
        if(sesionActive.equals("true")){
            var intent = Intent(this, home ::class.java)
            startActivity(intent)
        }

        btnSend.setOnClickListener{
            if(validator(txtUser.text.toString(),txtPass.text.toString())){
                var intent = Intent(this, home ::class.java)
                var name = txtUser.text.toString()
                //intent.putExtra("name", name)
                var editor = getSharedPreferences("dataUser", MODE_PRIVATE).edit()
                editor.putString("user",name)
                if ( sesion.isChecked){
                    editor.putString("sesionUser", "true")
                }
                editor.commit()
                startActivity(intent)
            }
            else{
                Toast.makeText(this, "Usuario o contraseña no validos", Toast.LENGTH_SHORT).show()
                txtUser.setText("")
                txtPass.setText("")
            }
        }

        txtSignIn.setOnClickListener{
            var intent = Intent(this, register ::class.java)
            startActivity(intent)
        }

    }



    private fun validator(user: String, password: String): Boolean{
        var prefs = getSharedPreferences("dataUser", MODE_PRIVATE)
        val name = prefs.getString("user", "no-data")
        val pass = prefs.getString("pass", "no-data")
        if(user.equals(name) && password.equals(pass)){
            return true
        }
        return false
    }
}
