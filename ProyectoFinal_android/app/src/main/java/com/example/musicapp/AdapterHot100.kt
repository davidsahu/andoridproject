package com.example.musicapp


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_hot100.view.*


class HotSaleAdapter (private val hotList: List<Hot100>) : RecyclerView.Adapter<HotSaleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HotSaleViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return HotSaleViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return hotList.size
    }

    override fun onBindViewHolder(holder: HotSaleViewHolder, position: Int) {
        val hotsale: Hot100 = hotList[position]
        holder.bind(hotsale)
    }




    inner class HotSaleAdapter(itemView: View): RecyclerView.ViewHolder(itemView){
        fun setData(hotsale: Hot100, pos: Int){
            itemView.txtProductH.text = hotsale.name
            itemView.txtDurationH.text = hotsale.duration.toString()
            itemView.txtBrandH.text = hotsale.brand
        }
    }

}

class HotSaleViewHolder (inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.card_hot100, parent, false)) {
    private var hTitleProduct: TextView? = null
    private var hPrice: TextView? = null
    private var hBrand: TextView? = null
    private var hImageProduct: ImageView? = null

    init {
        hTitleProduct = itemView.findViewById(R.id.txtProductH)
        hPrice = itemView.findViewById(R.id.txtDurationH)
        hBrand = itemView.findViewById(R.id.txtBrandH)
        hImageProduct = itemView.findViewById(R.id.imgProductH)
    }

    fun bind(hot100List: Hot100){
        hTitleProduct?.text = hot100List.name
        hPrice?.text = hot100List.duration.toString()
        hBrand?.text = hot100List.brand
        Picasso.get()
            .load( hot100List.img)
            .into(hImageProduct)
    }



}

