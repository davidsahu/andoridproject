package com.example.musicapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class profile : AppCompatActivity() {

    private lateinit var txtName : TextView
    private lateinit var btnClose: Button
    private lateinit var btnLogout: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        txtName = findViewById(R.id.txtName)
        btnClose = findViewById(R.id.btnClose)
        btnLogout = findViewById(R.id.btnLogout)
        var prefs = getSharedPreferences("dataUser", MODE_PRIVATE)
        val usuario = prefs.getString("user", "no-data")
        txtName.setText(usuario)

        btnClose.setOnClickListener{
            this.finish()
        }

        btnLogout.setOnClickListener{
            var intent = Intent(this, MainActivity ::class.java)
            var editor = getSharedPreferences("dataUser", MODE_PRIVATE).edit()
            editor.putString("sesionUser", "false")
            editor.commit()
            startActivity(intent)
        }
    }
}
