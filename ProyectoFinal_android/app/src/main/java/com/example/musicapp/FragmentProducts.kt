package com.example.musicapp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_products.*

class FragmentProducts : Fragment() {

    private val baselineProducts = listOf(
        Product("1", "High Hopes", "3:17 min", "Panic! At The Disco", "http://naciongrita.com.mx/wp-content/uploads/2018/03/PATD.jpg"),
        Product("2", "Hey Look Ma, I Made It",  "3:06min", "Panic! At The Disco","http://naciongrita.com.mx/wp-content/uploads/2018/03/PATD.jpg"),
        Product("3", "Natural", "3:10 min", "Imagine Dragons", "https://upload.wikimedia.org/wikipedia/en/thumb/1/10/Imagine_Dragons_Natural.png/220px-Imagine_Dragons_Natural.png"),
        Product("4", "Chlorine", "5:24 min", "twenty one pilots", "https://img.discogs.com/Z71XSro-dlLT9IYQVQAN6O28QKM=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-13001168-1546892886-9200.jpeg.jpg"),
        Product("5", "Bad Liar", "4:44 min", "Imagine Dragons", "https://i.pinimg.com/originals/0a/fa/62/0afa628528a62ebc8b594ff0c40bda68.jpg"),
        Product("6", "Blue On Black", "4:35 min", "Five Finger Death Punch", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQlOsix99BKJk0JHYngoQky8sMVtqirGf4PzSTypPXy54jpRNjO_w"),
        Product("7", "Lo/Hi", "3:44 min", "The Black Keys", "https://t2.genius.com/unsafe/168x0/https%3A%2F%2Fimages.genius.com%2Fc2cd9fdf397701884e4dee069bbf93cb.1000x1000x1.png"),
        Product("8", "Ready To Let Go", "3:15 min", "Cage The Elephant", "https://images.genius.com/e3bf6383792e6a9aa19db4e7a214c34d.1000x1000x1.jpg")

    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    companion object {
        fun newInstance(): FragmentProducts = FragmentProducts()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_products, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvProducts.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = AdapterProducts(baselineProducts)
        }

    }

}