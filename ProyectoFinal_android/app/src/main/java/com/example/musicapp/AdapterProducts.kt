package com.example.musicapp

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import kotlinx.android.synthetic.main.card_product.view.*
import android.widget.TextView
import com.squareup.picasso.Picasso
class AdapterProducts(private val productList: List<Product>) : RecyclerView.Adapter<ProductViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ProductViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product: Product = productList[position]
        holder.bind(product)
    }




    inner class ProductAdapter(itemView: View): RecyclerView.ViewHolder(itemView){
        fun setData(product: Product, pos: Int){
            itemView.txtProduct.text = product.name
            itemView.txtDuration.text = product.duration.toString()
            itemView.txtBrand.text = product.brand
        }
    }




}

class ProductViewHolder (inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.card_product, parent, false)) {
    private var pTitleProduct: TextView? = null
    private var pDuration: TextView? = null
    private var pBrand: TextView? = null
    private var pImageProduct: ImageView? = null

    init {
        pTitleProduct = itemView.findViewById(R.id.txtProduct)
        pDuration = itemView.findViewById(R.id.txtDuration)
        pBrand = itemView.findViewById(R.id.txtBrand)
        pImageProduct = itemView.findViewById(R.id.imgProduct)
    }

    fun bind(product: Product){
        pTitleProduct?.text = product.name
        pDuration?.text = product.duration.toString()
        pBrand?.text = product.brand
        Picasso.get()
            .load( product.img)
            .into(pImageProduct)
    }



}