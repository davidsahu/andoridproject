package com.example.musicapp

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast

class home : AppCompatActivity() {

    private lateinit var tabs: TabLayout
    private lateinit var viewpager: ViewPager
    // private lateinit var fbEjemplo: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        tabs = findViewById(R.id.tabs)
        viewpager = findViewById(R.id.viewpager)

        val adapter = ParentPagerAdapter(supportFragmentManager)
        viewpager.adapter = adapter
        tabs.setupWithViewPager(viewpager)
    }

    //setting menu in action bar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.my_menu,menu)
        if (supportActionBar != null) {
            val actionBar = supportActionBar
            actionBar!!.setDisplayHomeAsUpEnabled(false)
        }
        return super.onCreateOptionsMenu(menu)
    }

    // actions on click menu items
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_print -> {
            // User chose the "Print" item
            Toast.makeText(this, "Print action", Toast.LENGTH_LONG).show()
            var intent = Intent(this, profile ::class.java)
            startActivity(intent)
            true
        }
        android.R.id.home -> {
            Toast.makeText(this, "Home action", Toast.LENGTH_LONG).show()
            true
        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }
}
