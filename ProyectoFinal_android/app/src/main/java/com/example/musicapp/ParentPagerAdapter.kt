package com.example.musicapp

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class ParentPagerAdapter(manager: FragmentManager): FragmentPagerAdapter(manager)  {
    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> FragmentProducts.newInstance()
            1 -> FragmentUsers.newInstance()
            2 -> FragmentFavoritosUsers.newInstance()
            else -> FragmentHot100.newInstance()
        }
    }

    // Tabs que vamos a tener
    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Top Rock"
            1 -> "Amigos Activos"
            2 -> "Amigos Favoritos"
            else -> "Hot 100"
        }
    }

}