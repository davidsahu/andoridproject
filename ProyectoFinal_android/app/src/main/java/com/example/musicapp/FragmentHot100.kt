package com.example.musicapp


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_hot100.*


class FragmentHot100 : Fragment() {


    private val baselineHotSale = listOf(
        Hot100("1", "Old Town Road", "3:20min", "Lil Nas X Featuring Billy Ray", "https://s3.amazonaws.com/media.thecrimson.com/photos/2019/04/14/200610_1337381.jpeg"),
        Hot100("2", "I Don't Care",  "3:33min", "Ed Sheeran & Justin Bieber","https://laverdadnoticias.com/__export/1557464121074/sites/laverdad/img/2019/05/09/ed-i-dont-care.png_423682103.png"),
        Hot100("3", "Bad Guy", "4:12min", "Billie Eilish", "https://www.mor.bo/wp-content/uploads/2019/04/when-we-all-fall-asleep-where-do-we-go.jpg"),
        Hot100("4", "Wow.", "3:56min", "Post Malone", "https://upload.wikimedia.org/wikipedia/en/thumb/5/5e/Post_Malone_-_Wow.png/220px-Post_Malone_-_Wow.png")

    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    companion object {
        fun newInstance(): FragmentHot100 = FragmentHot100()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_hot100, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvHotSale.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = HotSaleAdapter(baselineHotSale)
        }

    }

}