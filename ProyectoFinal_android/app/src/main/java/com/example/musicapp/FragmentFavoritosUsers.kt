package com.example.musicapp


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import kotlinx.android.synthetic.main.fragment_users.*
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.fragment_users.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject


class FragmentFavoritosUsers : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    companion object {
        fun newInstance(): FragmentFavoritosUsers = FragmentFavoritosUsers()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_users, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvUsers.apply {
            layoutManager = LinearLayoutManager(activity)
            getUsersFromJSON2()
        }



    }

    // class Movie(var id: Int, var title: String, var original_title: String, var vote_average: Double) {}
    private fun getUsersFromJSON() {
        doAsync {
            val requestQueue: RequestQueue = Volley.newRequestQueue(context)
            var strRequest = object : StringRequest(
                Method.GET,
                "https://reqres.in/api/users?page=2",
                Response.Listener { s ->
                    Log.e("TAG", s.toString())

                    val jsonResponse = JSONObject(s)

                    var baselineUsers = mutableListOf<User>()
                    var i = 0
                    while ( i <= 2){
                        baselineUsers.add(i, User(
                            jsonResponse.getJSONArray("data").getJSONObject(i).getInt("id"),
                            jsonResponse.getJSONArray("data").getJSONObject(i).getString("first_name"),
                            jsonResponse.getJSONArray("data").getJSONObject(i).getString("last_name"),
                            jsonResponse.getJSONArray("data").getJSONObject(i).getString("email"),
                            jsonResponse.getJSONArray("data").getJSONObject(i).getString("avatar")
                        ))
                        i++
                    }

                    val adapter = AdapterUsers(baselineUsers)
                    rvUsers.adapter = adapter
                },
                Response.ErrorListener { e ->
                    Log.e("TAG", e.toString())
                }) {}
            requestQueue.add(strRequest)

        }

    }


    private fun getUsersFromJSON2() {
        doAsync {
            val requestQueue: RequestQueue = Volley.newRequestQueue(context)
            var strRequest = object : StringRequest(
                Method.GET,
                "https://reqres.in/api/users?page=1",
                Response.Listener { s ->
                    Log.e("TAG", s.toString())

                    val jsonResponse = JSONObject(s)

                    var baselineUsers2 = mutableListOf<User>()
                    var i = 0
                    while ( i <= 2){
                        baselineUsers2.add(i, User(
                            jsonResponse.getJSONArray("data").getJSONObject(i).getInt("id"),
                            jsonResponse.getJSONArray("data").getJSONObject(i).getString("first_name"),
                            jsonResponse.getJSONArray("data").getJSONObject(i).getString("last_name"),
                            jsonResponse.getJSONArray("data").getJSONObject(i).getString("email"),
                            jsonResponse.getJSONArray("data").getJSONObject(i).getString("avatar")
                        ))
                        i++
                    }

                    val adapter = AdapterUsers(baselineUsers2)
                    rvUsers.adapter = adapter
                },
                Response.ErrorListener { e ->
                    Log.e("TAG", e.toString())
                }) {}
            requestQueue.add(strRequest)

        }

    }
}