package com.example.musicapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class register : AppCompatActivity() {

    private lateinit var txtUser : EditText
    private lateinit var txtPass : EditText
    private lateinit var txtCorreo : EditText
    private lateinit var txtNumber : EditText
    private lateinit var btnRegister : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        initElements()
    }

    fun initElements(){
        txtUser = findViewById(R.id.txtUser)
        txtPass = findViewById(R.id.txtPass)
        txtCorreo = findViewById(R.id.txtCorreo)
        txtNumber = findViewById(R.id.txtNumber)
        btnRegister = findViewById(R.id.btnRegistro)

        btnRegister.setOnClickListener{
            if(dataValidation(txtUser.text.toString(),txtPass.text.toString(),txtCorreo.text.toString(),txtNumber.text.toString())){
                var editor = getSharedPreferences("dataUser", MODE_PRIVATE).edit()
                editor.putString("user",txtUser.text.toString())
                editor.putString("pass", txtPass.text.toString())
                editor.putString("city", txtCorreo.text.toString())
                editor.putString("age",txtNumber.text.toString())
                editor.commit()
                Toast.makeText(this, "Registro exitoso", Toast.LENGTH_SHORT).show()
                this.finish()
            }
            else{
                Toast.makeText(this, "No puedes dejar campos vacíos", Toast.LENGTH_SHORT).show()
            }
        }

    }

    fun dataValidation(user:String, pass: String, city: String, age: String): Boolean{
        if( user.length >0 && pass.length >0 && city.length > 0 && age.length >0){
            return true
        }
        return false
    }
}
